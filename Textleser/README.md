# Textleser

This directory contains the source code for Textleser (text reader),
my earliest software project that wasn't just some example code.
It has been written in the year 2002 using Turbo Pascal.

The source code uses german variables and mostly german text strings,
except for english versions of the program which are just copies of the
german source file with all text strings translated. To keep the files
in their original state, the Latin-1 encoding of the source code
is preserved. If you encounter encoding problems, you can convert the
source code to UTF-8 using iconv.

## Pre-release versions

The first pre-release version of Textleser is in the file
TEXT_MSS.PAS, which just reads 32 lines of text without saving them.

The next pre-release version is written in MSS_TE1D.PAS, a
"demo version" that just displays the menu without doing anything.


## Textleser 1 (TL1D.PAS)

Textleser 1 is the first release version of Textleser. It stores its
configuration in the files C:\B.TLB (user name and login code) and
C:\F.TLB (colours). These files are created by the code in TL1V.PAS,
the preparation program for the real Textleser program.

If you start Textleser, you must enter the login code.

After you "logged in", you get a menu with different actions that can be
executed. When writing text, you first enter the author and title and then
you can enter exactly 16 lines of text. Note that saving text is a different
action than entering text.

The meaning of the source code files is as follows:

- TL1D.PAS contains the original Textleser 1.0 program.
- TL1D_ENG.PAS is the english version.
- TL1V.PAS contans the preparation program that sets up the configuration files.
- TL1.PAS contains an extended version of Textleser 1.0 that already identifies
  itself as "Textleser 2.0 Demo". In addition to the standard 16 lines of text,
  it allows entering text with 32 lines and had options to enter "source code"
  and "music". Those options were never realised, but would probably have meant
  to build an interpreter for the source code and run it in Textleser and to
  output sound using the PC speaker.


## Textleser 2 (TL2.PAS)

This is the release version of Textleser 2.0. It doesn't have the option to
enter source code or music, but instead allows entering "security text"
that is protected by one password (although the program reports that two
passwords protect the data). The difference between a normal Textleser text
file and a "security text" is that the text lines in the latter are scrambled
together with the password, using a prefedined scrambling "order". After a
"security text" file is written, Textleser 2 reports that the file can be
decoded using the "security text decoder", that would be released in the mid
of 2002.

Textleser 2 has its own preparation program in KT1V.PAS which creates two
configuration files in C:\F.KTB (colours) and C:\B.KTB, which contains the
user name, some random data (which should be the version name)
and the login code. Note that KT1V reports itself as the preparation program
for "Kurztext 2.0" (short text 2.0) which is probably a planned alternative
name for Textleser 2.
